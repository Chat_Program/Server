﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;


namespace ConsoleClient
{
    class Program
    {
        static List<Client> clientList = new List<Client>();
        
        static void Main(string[] args)
        {
            try
            {
                
                IPAddress ipAd = IPAddress.Parse("123") ;
                int port = 0;
                Ask(ref ipAd, ref port);

                /* Initializes the Listener */
                TcpListener server = new TcpListener(ipAd, port);

                /* Start Listening at the specified port */
                server.Start();
                
                Console.WriteLine("\nWaiting for a connection.....");
                while (true)
                {
                    Socket socket = server.AcceptSocket();
                    Console.WriteLine("\nAccepted connection from {0}...", socket.RemoteEndPoint);

                    Task.Factory.StartNew(() =>
                    {
                        Listen(socket);

                    });
                    
                }
               
            }
            catch (Exception e)
            {
                Console.WriteLine("\nError " + e);
            }
            Console.ReadLine();
        }

        static void Ask(ref IPAddress ip, ref int port)
        {
            while (true)
            {
                Console.WriteLine("\nUse localhost:8080? y/n");
                string ans = Console.ReadLine();
                if (ans == "y")
                {
                   
                    ip = Dns.GetHostEntry("localhost").AddressList[0];
                    port = 8080;
                    return;
                }
                else if(ans == "n")
                {
                    while (true) {
                        Console.WriteLine("\nThis machine's ip? y/n");
                        ans = Console.ReadLine();
                        if (ans == "y")
                        {
                            ip = Array.Find(
                            Dns.GetHostEntry(string.Empty).AddressList,
                            a => a.AddressFamily == AddressFamily.InterNetwork);
                            Console.WriteLine("\nEnter port:");
                            port = Int32.Parse(Console.ReadLine());
                            return;
                        }
                        else if (ans == "n")
                        {
                            Console.WriteLine("\nEnter ip:");
                            ip = IPAddress.Parse(Console.ReadLine());
                            Console.WriteLine("\nEnter port:");
                            port = Int32.Parse(Console.ReadLine());
                            return;

                        }
                    }
                }
            }
        }
        
        static string MakeJsonString(string name, string message)
        {
            Message jsonObj = new Message
            {
                content = message,
                name = name
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj);
        }

        static void Listen(Socket socket)
        {
            Client client = new Client(socket, "");
            bool firstRun = true;
            while (true)
            {

                byte[] receiveBuffer = new byte[1024];
                UnicodeEncoding unicodeEnc = new UnicodeEncoding();
                try
                {
                    int bytesRec = socket.Receive(receiveBuffer);
                    Array.Resize(ref receiveBuffer, bytesRec);
                }
                catch
                {
                    Console.WriteLine("\n{0} ({1}) disconnected", client.name, client.socket.RemoteEndPoint);
                    clientList.Remove(client);
                    foreach (Client c in clientList)
                    {

                        c.socket.Send(unicodeEnc.GetBytes(MakeJsonString("Server", String.Format("\"{0}\" has left the chat.", client.name))));

                    }
                    return;
                }

                string receivedMsg = Encoding.Unicode.GetString(receiveBuffer);
                
                
                if (!firstRun)
                {
                    Console.WriteLine("\nReceived from {0}:", client.name);
                    Console.WriteLine("\n\"{0}\"",receivedMsg);

                    string jsonString = MakeJsonString(client.name, receivedMsg);
                    foreach (Client c in clientList)
                    {
                        if (c.socket != socket)
                        {
                            c.socket.Send(unicodeEnc.GetBytes(jsonString));
                            Console.WriteLine("\nSent message to {0} at {1}", c.name, c.socket.RemoteEndPoint);

                        }
                    }
                }
                else
                {
                    firstRun = false;
                    client = new Client(socket, receivedMsg);
                    clientList.Add(client);
                    
                    foreach (Client c in clientList)
                    {

                        c.socket.Send(unicodeEnc.GetBytes(MakeJsonString("Server", String.Format("\"{0}\" has joined the chat!", client.name))));
                    
                    }
                }

            }
        }

     
    }
    class Client
    {
        public readonly Socket socket;
        public readonly string name;
        public Client(Socket _socket, string _name)
        {
            socket = _socket;
            name = _name;

        }
    }

    class Message
    {
        public string content;
        public string name;
    }

}

